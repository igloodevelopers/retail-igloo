﻿namespace RoomBuildingStarterKit.UI
{
    using System;
    using UnityEngine;
    using RoomBuildingStarterKit.BuildSystem;
    using Igloo.Common;

    [RequireComponent(typeof(Animator))]
    public class SecretShopperMenu : Common.Singleton<SecretShopperMenu>
    {
        /// <summary>
        /// The animator component.
        /// </summary>
        private Animator animator;

        /// <summary>
        /// The help panel.
        /// </summary>
        public GameObject HelpPanel;

        /// <summary>
        /// The Igloo player component, to save referencing through Igloo manager. 
        /// </summary>
        private PlayerManager iglooPlayer { get => IglooManager.instance.PlayerManager; }

        /// <summary>
        /// The current piece of furniture that is selected. Can be null.
        /// </summary>
        private FurnitureController currentSelectedFurnitureController;

        /// <summary>
        /// The product list controller
        /// </summary>
        public ProductListController productListController;

        /// <summary>
        /// The game name logo image
        /// </summary>
        public GameObject GameNameLogo;

        /// <summary>
        /// The menu transition complete callback.
        /// </summary>
        private Action menuTransitionCallback;

        /// <summary>
        /// Executes when the fade out animation completed.
        /// </summary>
        public void OnFadeOutCompleted()
        {
            this.menuTransitionCallback?.Invoke();
        }

        /// <summary>
        /// Executes when back is selected
        /// </summary>
        public void OnBackButtonSelected()
        {
            // Turn off the help guide
            this.HideHelpUI();
            // Stop the igloo player moving about.
            iglooPlayer.movementMode = PlayerManager.MOVEMENT_MODE.FLYING_GHOST;

            iglooPlayer.GetComponent<CharacterController>().enabled = false;

            // Tell the game manager we're back in menu mode
            GameManager.inst.SetupMainMenuMode();

            // set the menu back to standard.
            MenuManager.inst.Menus[Menus.InGame_SecretShopper].SetActive(false);
            MenuManager.inst.Menus[Menus.ModeSelector].SetActive(true);

        }

        /// <summary>
        /// Executes when gameObject instantiates.
        /// </summary>
        private void Awake()
        {
            this.animator = this.GetComponent<Animator>();
            // Set igloo to walking mode & enable character controller. 
            iglooPlayer.movementMode = PlayerManager.MOVEMENT_MODE.WALKING;
            iglooPlayer.GetComponent<CharacterController>().enabled = true;
            this.ShowHelpUI();
        }

        /// <summary>
        /// Enables the product list menu for a selected Furniture.
        /// Called from PlayerPointer when product is selected
        /// </summary>
        public void ShowProductListMenu(FurnitureController selectedFurniture)
        {
            productListController.selectedFurnitureController = selectedFurniture;
            productListController.gameObject.SetActive(true);

            GameNameLogo.SetActive(false);
        }

        /// <summary>
        /// Hides the product list menu, and 0's everything
        /// Called by the menu itself when a button is selected
        /// </summary>
        public void HideProductListMenu()
        {
            
            productListController.selectedFurnitureController = null;
            productListController.gameObject.SetActive(false);
            GameNameLogo.SetActive(true);
        }

        /// <summary>
        /// Shows Help Guide
        /// </summary>
        public void ShowHelpUI()
        {
            if (this.HelpPanel.activeSelf == false)
            {
                this.HelpPanel.SetActive(true);
            }
        }

        /// <summary>
        /// Hides Help Guide
        /// </summary>
        public void HideHelpUI()
        {
            if (this.HelpPanel.activeSelf == true)
            {
                this.HelpPanel.SetActive(false);
            }
        }
    }
}
