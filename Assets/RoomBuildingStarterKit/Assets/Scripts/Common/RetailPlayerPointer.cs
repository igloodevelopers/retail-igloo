﻿using RoomBuildingStarterKit.BuildSystem;
using RoomBuildingStarterKit.UI;
using UnityEngine;

namespace Igloo.Common
{
    /// <summary>
    /// Retail Player Pointer Override class
    public class RetailPlayerPointer : PlayerPointer
    {
        /// <summary>
        /// The current selected furniture controller. Can be null.
        /// </summary>
        internal FurnitureController selectedObject = null;

        /// <summary>
        /// An overriden Update function.
        /// </summary>
        public override void Update()
        {
            base.Update();
            if(hit.transform != null && Input.GetButtonUp("Fire1"))
            {
                if (hit.transform.GetComponentInParent<FurnitureController>()
                      && GameManager.inst.CurrentGameMode == GameManager.GameMode.SecretShopper)
                {
                    // Hit a furnitureController, set it as selected object.
                    selectedObject = hit.transform.GetComponentInParent<FurnitureController>();
                    // Only do stuff, if the product type can be changed (Standard and Veg only)
                    if (selectedObject != null && selectedObject.ProductTypes[0] == ProductType.Standard || selectedObject.ProductTypes[0] == ProductType.Vegetables)
                    {
                        // Toggle on the product menu, and pass in the selected furniture controller.
                        MenuManager.inst.Menus[Menus.InGame_SecretShopper].GetComponent<SecretShopperMenu>().ShowProductListMenu(selectedObject);
                        // 
                    }

                }
            }

        }

        /// <summary>
        /// Called by pressed Mouse 1, or via various scripts from RoombuildingKit
        /// </summary>
        public void DeselectCurrentObject()
        {
            selectedObject = null;
        }
    }
}
