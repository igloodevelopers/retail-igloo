﻿namespace RoomBuildingStarterKit.UI
{
    using System;
    using UnityEngine;
    using RoomBuildingStarterKit.BuildSystem;

    [RequireComponent(typeof(Animator))]
    public class ProductEditorMenu : MonoBehaviour
    {
        /// <summary>
        /// The animator component.
        /// </summary>
        private Animator animator;


        /// <summary>
        /// The menu transition complete callback.
        /// </summary>
        private Action menuTransitionCallback;

        /// <summary>
        /// Executes when the fade out animation completed.
        /// </summary>
        public void OnFadeOutCompleted()
        {
            this.menuTransitionCallback?.Invoke();
        }

        /// <summary>
        /// Executes when back is selected
        /// </summary>
        public void OnBackButtonSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.ProductEditor].SetActive(false);
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(true);
            };
        }

        /// <summary>
        /// Executes when gameObject instantiates.
        /// </summary>
        private void Awake()
        {
            this.animator = this.GetComponent<Animator>();
        }
    }
}

