﻿namespace RoomBuildingStarterKit.Common
{
    using UnityEngine;
    using Igloo.Common;

    /// <summary>
    /// The Input wrapper class for handle UI block mouse event.
    /// </summary>
    public class InputWrapper
    {
        /// <summary>
        /// Whether the Input event is blocking.
        /// </summary>
        public static bool IsBlocking = false;

        /// <summary>
        /// Gets the mouse position.
        /// </summary>
        public static Vector3 MousePosition()
        {
            PlayerPointer.instance.CastRay(LayerMask.GetMask("Floor"), out RaycastHit hit);
            return hit.point;
        }

        /// <summary>
        /// Gets button key hold on.
        /// </summary>
        /// <param name="key">The button key.</param>
        /// <returns>True or false.</returns>
        //public static bool GetKey(KeyCode key)
        //{
        //    return InputWrapper.IsBlocking || UIMouseEventDetector.CheckMouseEventOnUI() ? false : Input.GetKey(key);
        //}

        public static bool GetButton(string btn)
        {
            return InputWrapper.IsBlocking || UIMouseEventDetector.CheckMouseEventOnUI() ? false : Input.GetButton(btn);
        }

        /// <summary>
        /// Gets the button key down.
        /// </summary>
        /// <param name="key">The button key.</param>
        /// <returns>True or false.</returns>
        //public static bool GetKeyDown(KeyCode key)
        //{
        //    return InputWrapper.IsBlocking || UIMouseEventDetector.CheckMouseEventOnUI() ? false : Input.GetKeyDown(key);
        //}
        public static bool GetButtonDown(string btn)
        {
            return InputWrapper.IsBlocking || UIMouseEventDetector.CheckMouseEventOnUI() ? false : Input.GetButtonDown(btn);
        }

        /// <summary>
        /// Gets the button key up.
        /// </summary>
        /// <param name="key">The button key.</param>
        /// <returns>True or false.</returns>
        //public static bool GetKeyUp(KeyCode key)
        //{
        //    return InputWrapper.IsBlocking || UIMouseEventDetector.CheckMouseEventOnUI() ? false : Input.GetKeyUp(key);
        //}
        public static bool GetButtonUp(string btn)
        {
            return InputWrapper.IsBlocking || UIMouseEventDetector.CheckMouseEventOnUI() ? false : Input.GetButtonDown(btn);
        }

        /// <summary>
        /// Gets axis.
        /// </summary>
        /// <param name="axisName">The axis name.</param>
        /// <returns>The axis input.</returns>
        public static float GetAxis(string axisName)
        {
            return InputWrapper.IsBlocking ? 0 : Input.GetAxis(axisName);
        }
    }
}