﻿namespace RoomBuildingStarterKit.UI
{
    using RoomBuildingStarterKit.BuildSystem;
    using RoomBuildingStarterKit.Common;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Assertions;
    using Igloo.Common;

    /// <summary>
    /// The Item list controller class.
    /// </summary>
    public class ProductListController : ItemListControllerBase
    {
        /// <summary>
        /// The shop list items.
        /// </summary>
        [SerializeField]
        public ChooseProductSubList ShopListItems;

        /// <summary>
        /// The currently selected piece of furniture.
        /// </summary>
        internal FurnitureController selectedFurnitureController;

        /// <summary>
        /// The product type.
        /// </summary>
        protected ProductType productType;

        /// <summary>
        /// Chooses the shop list.
        /// </summary>
        protected override void ChooseShopList()
        {
            productType = selectedFurnitureController.ProductTypes[0];
            shopListType = ShopListItems.Items.First(s => s.ShopList.ProductType == productType).ShopList;
        }

        /// <summary>
        /// Registers buttons.
        /// </summary>
        protected override void RegisterButtons()
        {
            Assert.IsTrue(this.itemButtons.Count == this.shopListType.ShopList.Items.Count);

            for (int i = 0; i < this.itemButtons.Count; ++i)
            {
                var item = this.shopListType.ShopList.Items[i];
                var newProduct = (int)item.ShopItemUIPrefab.GetComponent<ProductSubTypeItem>().ProductSubType;
                this.itemButtons[i].onClick.AddListener(() => this.OnButtonClicked(newProduct));
            }
        }

        /// <summary>
        /// Executes when buy furniture button clicked.
        /// </summary>
        /// <param name="i">The furniture index.</param>
        protected override void OnButtonClicked(int i)
        {
            selectedFurnitureController.ChangeProductSubType((ProductSubType)i);
            PlayerPointer.instance.GetComponent<RetailPlayerPointer>().DeselectCurrentObject();
            MenuManager.inst.Menus[Menus.InGame_SecretShopper].GetComponent<SecretShopperMenu>().HideProductListMenu();
        }

        /// <summary>
        /// Executes when close button clicked.
        /// </summary>
        protected override void OnCloseButtonClicked()
        {
            MenuManager.inst.Menus[Menus.InGame_SecretShopper].GetComponent<SecretShopperMenu>().HideProductListMenu();
        }
    }
}
