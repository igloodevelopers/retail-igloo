﻿namespace RoomBuildingStarterKit.BuildSystem
{
    using UnityEngine;
    /// <summary>
    /// The ProductGroupItem class used to control item behaviour, when interacting with Furniture.
    /// </summary>
    public class ProductGroupItem : MonoBehaviour
    {
        /// <summary>
        /// The product sub type of this item.
        /// </summary>
        public ProductSubType ProductSubType;
    }
}
