﻿namespace RoomBuildingStarterKit.BuildSystem
{
    using RoomBuildingStarterKit.Common;
    using Igloo.Common;
    using UnityEngine;
    using System.Collections;

    /// <summary>
    /// The Game Manager Class
    /// </summary>
    public class GameManager : SingletonGameObject<GameManager>
    {
        /// <summary>
        /// Game
        /// </summary>
        public enum GameMode
        {
            SecretShopper,
            StoreDesigner,
            ProductPlacement,
            ProductDesigner,
            MainMenu
        }

        /// <summary>
        /// The igloo player manager
        /// </summary>
        public PlayerManager player;

        /// <summary>
        /// The Current Came Mode.
        /// </summary>
        public GameMode CurrentGameMode = GameMode.MainMenu;

        /// <summary>
        /// Arrays of items that are to be toggled on/off when modes are switched.
        /// </summary>
        public GameObject[] SecretShopperToggleItems, StoreDesignerToggleItems, ProductPlacementToggleItems, ProductDesignerToggleItems, MainMenuToggleItems;

        /// <summary>
        /// Transforms for the Igloo Camera system to snap to or follow, depending on game state.
        /// </summary>
        [Header("Camera Follow Locations")]
        public Transform MenuCameraFollowLocation;
        public Transform SecretShopperStartLocation, ProductPlacementStartLocation, StorePlannerStartLocation;

        private FollowObjectTransform iglooFollow;
        
        /// <summary>
        /// Executes when the gameObject instantiates.
        /// </summary>
        protected override void AwakeInternal()
        {
            DontDestroyOnLoad(this.gameObject);
            SnapToCinematicCamera();
        }

        /// <summary>
        /// Sets up the player in secret shopper mode
        /// Minimal UI system
        /// Allows them to interact with shelves and products, to tag them. 
        /// Turns off interaction with all other systems.
        /// </summary>
        public void SetupSecretShopperMode()
        {
            ChangeGameMode(GameMode.SecretShopper);
            StartCoroutine(DisconnectCinematicCamera(SecretShopperStartLocation, false));
            InputWrapper.IsBlocking = true;
        }

        /// <summary>
        /// Sets up Store Designer mode. 
        /// Puts the player above the store
        /// Removes the roof, and anything that may obstruct the player from seeing the store layout. 
        /// Changes the Igloo Player system to being a Top Down system.
        /// Sets up the UI for store designer mode.
        /// </summary>
        public void SetupStorePlannerMode()
        {
            ChangeGameMode(GameMode.StoreDesigner);
            StartCoroutine(DisconnectCinematicCamera(StorePlannerStartLocation, true));
            InputWrapper.IsBlocking = false;
        }

        /// <summary>
        /// Sets up the Product placement mode. 
        /// places the player in first person mode, in the store. 
        /// Allows the player to select shelves, and place products from a large dictionary
        /// Stops the player from selecting anything other than the shelves, and their products. 
        /// </summary>
        public void SetupProductPlacementMode()
        {
            ChangeGameMode(GameMode.ProductPlacement);
            StartCoroutine(DisconnectCinematicCamera(ProductPlacementStartLocation, false));
        }

        /// <summary>
        /// Places the player in the correct location, 
        /// Turns on the product designer UI
        /// Turns off any interaction with the world.
        /// Sets up the product designer system
        /// </summary>
        public void SetupProductEditorMode()
        {
            ChangeGameMode(GameMode.ProductDesigner);
        }

        public void SetupMainMenuMode()
        {
            SnapToCinematicCamera();
            ChangeGameMode(GameMode.MainMenu);
        }

        private void SnapToCinematicCamera()
        {
            if (!iglooFollow) iglooFollow = player.GetComponent<FollowObjectTransform>();
            
            if (!iglooFollow.enabled) iglooFollow.enabled = true;

            iglooFollow.followObject = MenuCameraFollowLocation.gameObject;
            iglooFollow.followRotation = true;
            player.PlayerFrozen = true;
            player.m_storeDesignerMode = false;
        }

        private IEnumerator DisconnectCinematicCamera(Transform NewStartLocation, bool storeDesignerMode)
        {
            if (!iglooFollow) iglooFollow = player.GetComponent<FollowObjectTransform>();
            // Disconnect from the follow object system
            iglooFollow.enabled = false;
            iglooFollow.followObject = null;
            iglooFollow.followRotation = false;
            // Move the player to the new start location over 3 seconds. 
            float time = 0;
            Vector3 startLocation = iglooFollow.transform.position;
            Quaternion startRotation = iglooFollow.transform.rotation;

            while(time < 3.0f)
            {
                iglooFollow.transform.SetPositionAndRotation(
                    Vector3.Lerp(startLocation, NewStartLocation.position, (time/3.0f)),
                    Quaternion.Lerp(startRotation, NewStartLocation.rotation, (time/3.0f))
                );
                time += Time.deltaTime;
                yield return null;
            }
            // Set the player to the postion, post lerp
            iglooFollow.transform.SetPositionAndRotation(NewStartLocation.position, NewStartLocation.rotation);
            // allow interaciton.
            player.PlayerFrozen = false;
            player.enabled = true;
            player.m_storeDesignerMode = storeDesignerMode;
        }

        private void ChangeGameMode(GameMode newGameMode)
        {
            ToggleObjects(true);
            CurrentGameMode = newGameMode;
            ToggleObjects(false);
        }

        private void ToggleObjects(bool toggleState)
        {
            GameObject[] toggleListItems = new GameObject[0]; 
            switch (CurrentGameMode)
            {
                case GameMode.MainMenu:
                    toggleListItems = MainMenuToggleItems;
                    break;
                case GameMode.ProductDesigner:
                    toggleListItems = ProductDesignerToggleItems;
                    break;
                case GameMode.ProductPlacement:
                    toggleListItems = ProductPlacementToggleItems;
                    break;
                case GameMode.SecretShopper:
                    toggleListItems = SecretShopperToggleItems;
                    break;
                case GameMode.StoreDesigner:
                    toggleListItems = StoreDesignerToggleItems;
                    break;

            }
            //Debug.Log($"Toggling object list : {CurrentGameMode.ToString()}, Length : {toggleListItems.Length}, State : {toggleState} ");
            for (int i = 0; i < toggleListItems.Length; i++)
            {
                if(toggleListItems[i] != null)
                    toggleListItems[i].SetActive(toggleState);
                else
                    Debug.LogWarning($"Missing Toggle List Item at Element {i}");
            }
        }
    }
}
