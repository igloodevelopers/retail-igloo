﻿namespace RoomBuildingStarterKit.Common
{
    using Newtonsoft.Json;
    using RoomBuildingStarterKit.Configurations;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using TMPro;
    using UnityEngine;

    /// <summary>
    /// The language definitions.
    /// </summary>
    public enum Language
    {
        ENGLISH,
        CHINESE,
    }

    /// <summary>
    /// The LanguageManager class.
    /// </summary>
    public class LanguageManager : Singleton<LanguageManager>
    {
        /// <summary>
        /// The language setting.
        /// </summary>
        public LanguageSetting LanguageSetting;

        /// <summary>
        /// The fonts.
        /// </summary>
        public List<TMP_FontAsset> Fonts = new List<TMP_FontAsset>();

        /// <summary>
        /// The language settings change callback.
        /// </summary>
        public Action OnChangeLanguageHandler = () => { };

        /// <summary>
        /// The multi-language texts.
        /// </summary>
        private Dictionary<string, Dictionary<string, string>> multiLanguageTexts;

        /// <summary>
        /// Gets multi-language text with specified font.
        /// </summary>
        /// <param name="text">The ui text enum.</param>
        /// <param name="font">The font.</param>
        /// <returns>The text string.</returns>
        public string GetText(UIText text, out TMP_FontAsset font)
        {
            font = this.Fonts[this.LanguageSetting.Index];
            return this.GetText(text);
        }

        /// <summary>
        /// Gets multi-language text.
        /// </summary>
        /// <param name="text">The ui text enum.</param>
        /// <returns>The text string.</returns>
        public string GetText(UIText text)
        {
            Dictionary<string, string> newDict = new Dictionary<string, string>();

            if(!multiLanguageTexts.TryGetValue(text.ToString(), out newDict))
            {
                Debug.LogWarning($"Could not find text: {text} in multiLanguageTexts");
            }

            if(!newDict.TryGetValue(LanguageSetting.Value.ToString(), out string newText))
            {
                Debug.LogWarning($"Could not find text: {LanguageSetting.Value.ToString()} in newDict");
            }
            return newText;
        }

        /// <summary>
        /// Executes when gameObject instantiates..
        /// </summary>
        protected override void AwakeInternal()
        {
            var content = File.ReadAllText($"{Application.streamingAssetsPath}\\Data\\GameText.json");
            this.multiLanguageTexts = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(content);
        }
    }
}
