﻿namespace RoomBuildingStarterKit.UI
{
    using System;
    using UnityEngine;
    using RoomBuildingStarterKit.BuildSystem;
    using UnityEngine.UI;

    public class ModeSelectionMenu : MonoBehaviour
    {
        /// <summary>
        /// The animator component.
        /// </summary>
        private Animator animator;

        /// <summary>
        /// The menu transition complete callback.
        /// </summary>
        private Action menuTransitionCallback;

        /// <summary>
        /// Executes when the fade out animation completed.
        /// </summary>
        public void OnFadeOutCompleted()
        {
            this.menuTransitionCallback?.Invoke();
        }

        /// <summary>
        /// Executes when secret shopper is selected
        /// </summary>
        public void OnSecretShopperModeSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.InGame_SecretShopper].SetActive(true);
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(false);
                GameManager.inst.SetupSecretShopperMode();
            };
        }

        /// <summary>
        /// Executes when save game button selected
        /// </summary>
        public void OnSaveGameSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.SaveLoadGameMenu].SetActive(true);
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(false);
            };
        }

        /// <summary>
        /// Executes when store planner is selected
        /// </summary>
        public void OnStorePlannerModeSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.InGame_StorePlanner].SetActive(true);
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(false);
                GameManager.inst.SetupStorePlannerMode();
            };
        }

        /// <summary>
        /// Executes when product editor mode is selected
        /// </summary>
        public void OnProductEditorModeSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.ProductEditor].SetActive(true);
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(false);
                GameManager.inst.SetupProductEditorMode();
            };
        }

        /// <summary>
        /// Executes when product planner mode is selected
        /// </summary>
        public void OnProductPlannerModeSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.InGame_ProductPlanner].SetActive(true);
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(false);
                GameManager.inst.SetupProductPlacementMode();
            };
        }

        /// <summary>
        /// Executes when back is selected
        /// </summary>
        public void OnBackButtonSelected()
        {
            this.animator.SetTrigger("Exit");
            this.menuTransitionCallback = () =>
            {
                MenuManager.inst.Menus[Menus.ModeSelector].SetActive(false);
                MenuManager.inst.Menus[Menus.StartMenu].SetActive(true);
                GameManager.inst.SetupMainMenuMode();
            };
        }

        /// <summary>
        /// Executes when gameObject instantiates.
        /// </summary>
        private void Awake()
        {
            this.animator = this.GetComponent<Animator>();
        }
    }
}
