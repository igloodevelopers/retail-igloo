﻿namespace RoomBuildingStarterKit.BuildSystem
{
    /// <summary>
    /// The furniture type definitions.
    /// </summary>
    public enum FurnitureType
    {
        Chiller_Isle,
        Chiller_Large,
        Chiller_Small,

        Standard_Small,
        Standard_Large,

        Flowers_Small,

        Bakery_Small,
        Bakery_Large,

        Frozen_Small,
        Frozen_Large,

        Pallet_Small,

        Wooden_Large,
        Wooden_Small,

        Common_Window,
        Common_Door,
        Common_OfficeDoor,

    }

    public enum ProductType
    {
        Vegetables, 
        Chilled,
        Frozen,
        Flowers,
        Pallet,
        Standard,
        Bakery,
    }

    public enum ProductSubType
    {
        // Default not applicable.
        Default_NA,

        //Vegetables
        Vegetables_Veg,
        Vegetables_Fruit,

        // Standard
        Standard_Toiletries, 
        Standard_Soft_Drinks, 
        Standard_Alcohol, 
        Standard_Kitchen, 
        Standard_Misc, 
        Standard_Food, 
    }
}