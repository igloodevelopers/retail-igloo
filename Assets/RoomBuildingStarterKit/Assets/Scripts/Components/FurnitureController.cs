﻿namespace RoomBuildingStarterKit.BuildSystem
{
    using RoomBuildingStarterKit.VisualizeDictionary.Implementations;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// The FurnitureController class used to control furniture behaviour.
    /// </summary>
    public class FurnitureController : MonoBehaviour
    {
        /// <summary>
        /// The furniture occupied floor size.
        /// </summary>
        public Vector2Int Dimension;

        /// <summary>
        /// The furniture type.
        /// </summary>
        public FurnitureType FurnitureType;

        /// <summary>
        /// The Products able to be placed.
        /// </summary>
        public ProductType[] ProductTypes;

        /// <summary>
        /// The current sub type of the products
        /// </summary>
        public ProductSubType ProductSubType = ProductSubType.Default_NA;

        /// <summary>
        /// The product type to prefab mapping file.
        /// </summary>
        public ProductTypeToPrefabMapping ProductTypeToPrefabMapping;

        /// <summary>
        /// Gets the product type to prefab mappings
        /// </summary>
        public ProductTypeEnumGameObjectDict ProductTypeToPrefabs
        {
            get => this.ProductTypeToPrefabMapping.ProductTypeEnumGameObjectDict;
        }

        /// <summary>
        /// Has the player selected this object.
        /// </summary>
        public bool IsSelected;

        /// <summary>
        /// Locations that Items can be placed.
        /// </summary>
        public Transform[] ItemPlacementLocations;

        /// <summary>
        /// The furniture is a wall furniture or not.
        /// </summary>
        public bool IsWallFurniture;

        /// <summary>
        /// The furniture type.
        /// </summary>
        public GameObject Furniture;

        /// <summary>
        /// The hint panel.
        /// </summary>
        public GameObject HintPanel;

        /// <summary>
        /// The green panel sprite.
        /// </summary>
        public Sprite GreenPanelSprite;

        /// <summary>
        /// The red panel sprite.
        /// </summary>
        public Sprite RedPanelSprite;

        /// <summary>
        /// The hint panel sprite render.
        /// </summary>
        private SpriteRenderer hintPanelSpriteRender;

        /// <summary>
        /// The wait for end of frame.
        /// </summary>
        private WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

        /// <summary>
        /// The smooth time.
        /// </summary>
        private float smoothTime = 0.1f;


        /// <summary>
        /// The color change smooth damp velocity.
        /// </summary>
        private float colorSmoothChangeVelocity;

        /// <summary>
        /// Shows the buildable panel.
        /// </summary>
        public void ShowBuildablePanel()
        {
            var renderer = this.HintPanel.GetComponent<SpriteRenderer>();
            renderer.sprite = this.GreenPanelSprite;

            if (this.HintPanel.activeSelf == false)
            {
                this.EnableBuildablePanel();
                StartCoroutine(this.ShowBuildablePanelSmooth(renderer));
            }
        }

        /// <summary>
        /// Sets the buildable state.
        /// </summary>
        /// <param name="isBuildable">Whether can build the furniture or not.</param>
        /// <param name="isPanelTransparent">Is a transparent panel or not.</param>
        public void SetBuildableState(bool isBuildable, bool isPanelTransparent = false)
        {
            if (isBuildable)
            {
                this.DisableBuildablePanel();
            }
            else
            {
                this.ShowNonBuildablePanel(isPanelTransparent);
            }
        }

        /// <summary>
        /// Shows the non buildable panel.
        /// </summary>
        /// <param name="shouldTransparent">Should the buildable panel be transparent or not.</param>
        public void ShowNonBuildablePanel(bool shouldTransparent = true)
        {
            var renderer = this.HintPanel.GetComponent<SpriteRenderer>();
            renderer.sprite = this.RedPanelSprite;

            if (this.HintPanel.activeSelf == false)
            {
                this.EnableBuildablePanel();
                StartCoroutine(this.ShowBuildablePanelSmooth(renderer));
            }
        }

        /// <summary>
        /// Disables the buildable panel.
        /// </summary>
        public void DisableBuildablePanel()
        {
            this.HintPanel.SetActive(false);
        }

        /// <summary>
        /// Enables the buildable panel.
        /// </summary>
        public void EnableBuildablePanel()
        {
            this.HintPanel.SetActive(true);
        }

        /// <summary>
        /// Shows buildable panel smoothly.
        /// </summary>
        /// <param name="renderer">The sprite renderer.</param>
        /// <returns>The coroutine.</returns>
        private IEnumerator ShowBuildablePanelSmooth(SpriteRenderer renderer)
        {
            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 0);
            while (renderer.color.a <= 0.999f)
            {
                yield return this.waitForEndOfFrame;
                renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, Mathf.SmoothDamp(renderer.color.a, 1f, 
                    ref this.colorSmoothChangeVelocity, this.smoothTime, float.MaxValue, Time.unscaledDeltaTime));
            }

            renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, 1f);
            StopAllCoroutines();
        }

        /// <summary>
        /// Executes when gameObject instantiates.
        /// </summary>
        private void Awake()
        {
            this.hintPanelSpriteRender = this.HintPanel.GetComponent<SpriteRenderer>();
            PopulateProducts();
        }

        /// <summary>
        /// Executes when object is placed. 
        /// Populates each object slot with products based on the selected product type.
        /// </summary>
        internal void PopulateProducts()
        {
            List<GameObject> possibleProducts = new List<GameObject>();
            for(int i = 0; i < ProductTypes.Length; i++)
            { 
                var prods = this.ProductTypeToPrefabs[ProductTypes[i]];
                // If sorting products based on type. only pick the specific products
                if(ProductSubType != ProductSubType.Default_NA)
                {
                    for(int p = 0; p < prods.Length; p++)
                    {
                        if(prods[p] && prods[p].GetComponent<ProductGroupItem>())
                            if(prods[p].GetComponent<ProductGroupItem>().ProductSubType == ProductSubType)
                            {
                                possibleProducts.Add(prods[p]);
                            }
                    }
                }
                else
                {
                    // all products of range
                    possibleProducts.AddRange(prods);
                }
            }

            foreach(Transform location in ItemPlacementLocations)
            {
                var randy = UnityEngine.Random.Range(0,  possibleProducts.Count - 1);
                if (!possibleProducts[randy]) return;
                var newObj = Instantiate(possibleProducts[randy], location) as GameObject;
                newObj.transform.localPosition = Vector3.zero;
                newObj.transform.localRotation = Quaternion.identity;
                newObj.transform.localScale = Vector3.one;
                //possibleProducts.RemoveAt(randy);
            }
        }

        /// <summary>
        /// Called by Player interaction, allows the productSubType to be changed to suit.
        /// Destroys all current products, changes productSubType, and replenishes products with SubType.
        /// </summary>
        internal void ChangeProductSubType(ProductSubType newProductSubType)
        {
            foreach (Transform location in ItemPlacementLocations)
            {
                if(location.childCount > 0)
                {
                    for(int i = 0; i < location.childCount; i++)
                    {
                        Destroy(location.GetChild(i).gameObject);
                    }
                }
            }
            ProductSubType = newProductSubType;
            PopulateProducts();
        }
    }
}