﻿namespace RoomBuildingStarterKit.BuildSystem
{
    using RoomBuildingStarterKit.VisualizeDictionary.Implementations;
    using UnityEngine;

    /// <summary>
    /// The ProductTypeToPrefabMapping class.
    /// </summary>
   [CreateAssetMenu(fileName = "ProductTypeToPrefabMapping", menuName = "BuildSystem/ProductTypeToPrefabMapping", order = 2)]
    public class ProductTypeToPrefabMapping : ScriptableObject
    {
        /// <summary>
        /// The product type dictionary <productTpe, GameObject>
        /// </summary>
        [ProductTypeEnumGameObjectDict]
        public ProductTypeEnumGameObjectDict ProductTypeEnumGameObjectDict;
    }
}