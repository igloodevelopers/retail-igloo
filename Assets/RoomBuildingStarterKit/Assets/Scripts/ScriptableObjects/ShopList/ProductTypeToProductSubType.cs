﻿namespace RoomBuildingStarterKit.Common
{
    using RoomBuildingStarterKit.BuildSystem;
    using System;
    using UnityEngine;

    /// <summary>
    /// The ShopListItem element used by choose shop list.
    /// </summary>
    [Serializable]
    public struct ProductListItem
    {
        /// <summary>
        /// The ShopList item.
        /// </summary>
        public ProductTypeToProductSubType ShopList;
    }

    /// <summary>
    /// The choose shop list used for reorderable list.
    /// </summary>
    [Serializable]
    public class ChooseProductSubList : ReorderableList<ProductListItem>
    {
    }


    /// <summary>
    /// The ProductShopList class.
    /// </summary>
    [Serializable]
    [CreateAssetMenu(fileName = "ProductTypeShopList", menuName = "ShopList/ProductTypeShopList", order = 2)]
    public class ProductTypeToProductSubType : ShopListBase
    {
        public ProductType ProductType = ProductType.Standard;
    }
}