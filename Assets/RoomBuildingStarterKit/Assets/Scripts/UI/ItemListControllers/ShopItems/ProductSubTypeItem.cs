﻿namespace RoomBuildingStarterKit.UI
{
    using RoomBuildingStarterKit.BuildSystem;

    public class ProductSubTypeItem : ShopItem
    {
        public ProductSubType ProductSubType;
    }
}