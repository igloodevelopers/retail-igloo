﻿namespace RoomBuildingStarterKit.UI
{
    using RoomBuildingStarterKit.Common;
    using RoomBuildingStarterKit.VisualizeDictionary.Implementations;

    /// <summary>
    /// The menu definitions.
    /// </summary>
    public enum Menus
    {
        StartMenu,
        SaveLoadGameMenu,
        SettingsMenu,
        LoadSceneMenu,
        PauseMenu,
        InGame_SecretShopper,
        InGame_StorePlanner,
        InGame_ProductPlanner,
        ProductEditor,
        ModeSelector,
    }

    /// <summary>
    /// The menu manager class.
    /// </summary>
    public class MenuManager : Singleton<MenuManager>
    {
        /// <summary>
        /// The menus.
        /// </summary>
        [MenuEnumGameObjectDict]
        public MenuEnumGameObjectDict Menus;
    }
}