﻿namespace RoomBuildingStarterKit.Editor
{
    using RoomBuildingStarterKit.BuildSystem;
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class RefreshProductTypetoPrefabMapping
    {
        /// <summary>
        /// Refreshes the product type to prefab mappings.
        /// </summary>
        [MenuItem("Tools/UpdateProductInfo/Refresh Product Type To Prefab Mappings", priority = 0)]
        public static void Refresh()
        {
            var mapping = AssetDatabase.LoadAssetAtPath($@"Assets/RoomBuildingStarterKit/Assets/ScriptableObjects/Mappings/ProductTypeToPrefabMapping.asset", typeof(ProductTypeToPrefabMapping)) as ProductTypeToPrefabMapping;
            mapping.ProductTypeEnumGameObjectDict.Clear();
            var vals = Enum.GetValues(typeof(ProductType));
            
            foreach(ProductType productType in vals)
            {
                var guids = AssetDatabase.FindAssets("", new[] { $@"Assets/RoomBuildingStarterKit/Products/{productType}" });
                
                List<GameObject> foundObjs = new List<GameObject>();
                foreach (var guid in guids)
                {
                    var path = AssetDatabase.GUIDToAssetPath(guid);
                    var obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject; 
                    if(obj != null)
                    {
                        foundObjs.Add(obj);
                    }
                }

                mapping.ProductTypeEnumGameObjectDict.Add(productType, foundObjs.ToArray());
            }

            EditorUtility.SetDirty(mapping);
        }
    }
}
